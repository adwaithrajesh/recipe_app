from flask import Flask
from flask import render_template
from flask import request
from flask import redirect

from database import DataBase

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def search_recipe():
    if request.method == 'GET':
        return render_template('index.html', recipes=[])

    if request.method == 'POST':
        query = request.form['query'].strip()

        with DataBase() as db:
            recipes = db.search_recipe(query)

        recipes = [(i, n, r.replace('\n', '<br>')) for i, n, r in recipes]
        return render_template('index.html', recipes=recipes)



@app.route('/add-recipe', methods=['POST', 'GET'])
def add_recipe():
    if request.method == 'GET':
        return render_template('add_recipe.html')
    elif request.method == 'POST':
        print(request.form['recipe_name'])
        print(request.form['recipe'])

        with DataBase() as db:
            db.add_recipe(request.form['recipe_name'], request.form['recipe'])

        return redirect('/', code=303)


@app.route('/delete/<rid>')
def delete_recipe(rid):
    with DataBase() as db:
        db.delete_recipe(int(rid))
    return redirect('/', code=303)