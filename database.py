from __future__  import annotations

import sqlite3
from types import TracebackType

INIT_QUERY = '''
CREATE TABLE IF NOT EXISTS recipes(r_id INTEGER PRIMARY KEY NOT NULL,
 recipe_name VARCHAR(25), recipe VARCHAR(2048))
'''

class DataBase:
    def __init__(self) -> None:
        self.conn = sqlite3.connect('db.db')
        self.curr = self.conn.cursor()
        self._init_db()

    def __enter__(self) -> DataBase:
        return self

    def __exit__(self, exec_type: type[BaseException] | None,
                    exec_val: BaseException | None, traceback: TracebackType | None):
        self.conn.close()


    def _init_db(self) -> None:
        self.curr.execute(INIT_QUERY)
        self.conn.commit()

    def search_recipe(self, recipe_name: str) -> list[tuple[str, str]]:
        self.curr.execute('SELECT * FROM recipes WHERE recipe_name LIKe ?',
                          (f'%{recipe_name}%',))
        return [i for i in self.curr.fetchall()]

    def add_recipe(self, recipe_name: str, recipe: str) -> None:
        self.curr.execute(
            'INSERT INTO recipes (recipe_name, recipe) VALUES(?, ?)',
            (recipe_name, recipe)
        )
        self.conn.commit()

    def delete_recipe(self, rid: int) -> None:
        self.curr.execute(
            'DELETE FROM recipes WHERE r_id=?',
            (rid,)
        )
        self.conn.commit()